public class FibonachyNumbers  {

    private int  number1;
    private int number2;
    private int quantityFiboNumbers;

    public int getNumber1() {
        return number1;
    }

    public void setNumber1(int number1) {
        this.number1 = number1;
    }

    public int getNumber2() {
        return number2;
    }

    public void setNumber2(int number2) {
        this.number2 = number2;
    }

    public int getQuantityFiboNumbers() {
        return quantityFiboNumbers;
    }

    public void setQuantityFiboNumbers(int quatityFiboNumbers) {
        this.quantityFiboNumbers = quatityFiboNumbers;
    }

    public static void printFiboNumbers (int  number1, int number2, int quantityFiboNumbers) {
        System.out.print("Ряд Фібоначчі: " + number1 + ", " + number2);
        int rezult;
        int quantityOdd = 1;
        int quantityEven = 1;
        quantityFiboNumbers -= 2;
        for (int i = 0; i < quantityFiboNumbers; i++) {
            rezult = number1 + number2;
            number1 = number2;
            number2 = rezult;
            System.out.print(", " + rezult);
            if (rezult % 2 == 0) {
                quantityEven++;
            } else {
                quantityOdd++;
            }
        }
    }
}