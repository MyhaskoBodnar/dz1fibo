public class Interval {

    private int  number1;
    private int number2;

    public Interval() {
    }

    public Interval(int number1, int number2) {
        this.number1 = number1;
        this.number2 = number2;
    }

    public int getNumber1() {
        return number1;
    }

    public void setNumber1(int number1) {
        this.number1 = number1;
    }

    public int getNumber2() {
        return number2;
    }

    public void setNumber2(int number2) {
        this.number2 = number2;
    }


    @Override
    public String toString(){
        return  ("You have entered the interval of numbers [" + number1 + ", " + number2 + "]");
    }


    public   static void printEvenNumbers (int number1, int number2){
        System.out.print("Even number range: ");

        for (int i = number2; i >= number1 ; i--) {
            if (i % 2 == 0)
                System.out.print(i + ", ");
        }
        System.out.println();
    }


    protected    static void printOddNumbers (int f1, int f2){
        System.out.print("Odd number range: ");

        for (int i = f1; i <= f2 ; i++) {
            if (i % 2 != 0)
                System.out.print(i + ", ");
        }
        System.out.println();
    }

    public  static int findMaxOdd (int f1, int f2){
        int maxOdd = 0;
        for (int i = f1; i <= f2 ; i++) {
            if (i % 2 != 0 && i > maxOdd){
                maxOdd = i;
            }
        }
        return maxOdd;
    }

    public  static int findMaxEven (int f1, int f2){
        int maxEven = 0;
        for (int i = f1; i <= f2 ; i++) {
            if (i % 2 == 0 && i > maxEven){
                maxEven = i;
            }
        }
        return maxEven;
    }

    public static void sumOdds (int f1, int f2){
        int sum = 0;
        for (int i = f1; i <= f2 ; i++) {
            if (i % 2 != 0)
                sum += i;
        }
        System.out.println("The sum of odd numbers - " + sum);
    }
    public static void sumEven (int f1, int f2){
        int sum = 0;
        for (int i = f1; i <= f2 ; i++) {
            if (i % 2 == 0)
                sum += i;
        }
        System.out.println("The sum of even numbers - " + sum);
    }
}
