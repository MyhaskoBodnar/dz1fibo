import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        Interval interval = new Interval();

        System.out.println("Enter the number interval: ");
        System.out.print("First number - ");
        int  number1 = scan.nextInt();

        System.out.print("Second number - ");
        int number2 = scan.nextInt();

        if (number1 <= number2)
        {
        interval.setNumber1(number1);
        interval.setNumber2(number2);
        }
            else {
            interval.setNumber1(number2);
            interval.setNumber2(number1);
        }

        System.out.println(interval);

        Interval.printEvenNumbers(interval.getNumber1(), interval.getNumber2());
        Interval.printOddNumbers(interval.getNumber1(), interval.getNumber2());
        Interval.sumEven(interval.getNumber1(), interval.getNumber2());
        Interval.sumOdds(interval.getNumber1(), interval.getNumber2());

        FibonachyNumbers fibonachyNumbers = new FibonachyNumbers();

        if (makeCorrectRange(interval)){
            fibonachyNumbers.setNumber1(Interval.findMaxEven(interval.getNumber1(), interval.getNumber2()));
            fibonachyNumbers.setNumber2(Interval.findMaxOdd(interval.getNumber1(), interval.getNumber2()));
        }
        else {
            fibonachyNumbers.setNumber2(Interval.findMaxEven(interval.getNumber1(), interval.getNumber2()));
            fibonachyNumbers.setNumber1(Interval.findMaxOdd(interval.getNumber1(), interval.getNumber2()));
        }

        System.out.print("Put quantity fibo numbers - ");

        fibonachyNumbers.setQuantityFiboNumbers (scan.nextInt());


        FibonachyNumbers.printFiboNumbers(fibonachyNumbers.getNumber1(), fibonachyNumbers.getNumber2(), fibonachyNumbers.getQuantityFiboNumbers());


    }
     private static boolean makeCorrectRange (Interval interval ){
         if(Interval.findMaxEven(interval.getNumber1(), interval.getNumber2()) < Interval.findMaxOdd(interval.getNumber1(), interval.getNumber2()))
        return true;
         else
             return false;
    }

}
